import java.util.Scanner;  // Import the Scanner class

public class Main{
    public static void main(String[] args) {
        mainMenu();
        int sisi, jari, alas, tinggi, panjang, lebar;
        String pilihanBangun, pilihanBidang;
        boolean flagLoop = true; // untuk kondisi awal while
        Scanner myObj = new Scanner(System.in);
        String pilihan = myObj.nextLine();
        while(flagLoop){
            switch(pilihan){
                case "1":
                    System.out.println("--------------------git-------------------");
                    System.out.println("Pilih bidang yang akan dihitung luasnya");
                    System.out.println("---------------------------------------");
                    System.out.println("1. Persegi");
                    System.out.println("2. Lingkaran");
                    System.out.println("3. Segitiga");
                    System.out.println("4. Persegi Panjang");
                    System.out.println("0. Kembali ke menu sebelumnya");
                    System.out.println("---------------------------------------");
                    System.out.print("Masukkan pilihan: ");
                    pilihanBidang = myObj.nextLine();
                    switch(pilihanBidang){
                        case "1":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung luas Persegi");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai sisi: ");
                            sisi = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Panjang dari Persegi adalah " + (sisi * sisi));
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "2":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung luas Lingkaran");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai jari jari: ");
                            jari = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Panjang dari Lingkaran adalah " + Math.PI*(jari*jari));
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "3":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung luas Segitiga");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai alas Segitiga: ");
                            alas = Integer.parseInt(myObj.nextLine());
                            System.out.print("Masukkan nilai tinggi Segitiga: ");
                            tinggi = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Panjang dari Segitiga adalah " + (alas * tinggi)/2);
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "4":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung luas Persegi Panjang");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai panjang Persegi Panjang: ");
                            panjang = Integer.parseInt(myObj.nextLine());
                            System.out.print("Masukkan nilai lebar Persegi Panjang: ");
                            lebar = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Panjang dari Persegi Panjang adalah " + panjang * lebar);
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "0":
                            pilihan = "3";
                            break;
                        default:
                            break;
                    }
                    break;
                case "2":
                    System.out.println("-----------------------------------------");
                    System.out.println("Pilih bidang yang akan dihitung volumenya");
                    System.out.println("-----------------------------------------");
                    System.out.println("1. Kubus");
                    System.out.println("2. Balok");
                    System.out.println("3. Tabung");
                    System.out.println("0. Kembali ke menu sebelumnya");
                    System.out.println("-----------------------------------------");
                    System.out.print("Masukkan pilihan: ");
                    pilihanBangun = myObj.nextLine();
                    switch(pilihanBangun){
                        case "1":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung volume kubus");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai sisi kubus: ");
                            sisi = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Volume dari kubus adalah " + Math.pow(sisi,3));
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "2":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung volume Balok");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai panjang Balok: ");
                            panjang = Integer.parseInt(myObj.nextLine());
                            System.out.print("Masukkan nilai lebar Balok: ");
                            lebar  = Integer.parseInt(myObj.nextLine());
                            System.out.print("Masukkan nilai tinggi Balok: ");
                            tinggi  = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Volume dari Balok adalah " + (panjang * lebar * tinggi));
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "3":
                            System.out.println("------------------------------------------");
                            System.out.println("Anda memilih menghitung volume Tabung");
                            System.out.println("------------------------------------------");
                            System.out.print("Masukkan nilai jari-jari Tabung: ");
                            jari = Integer.parseInt(myObj.nextLine());
                            System.out.print("Masukkan nilai tinggi Tabung: ");
                            tinggi = Integer.parseInt(myObj.nextLine());
                            System.out.print("\n");
                            System.out.println("Proses...");
                            System.out.print("\n");
                            System.out.println("Volume dari Tabung adalah " + (Math.PI * tinggi * (jari*jari)));
                            System.out.println("------------------------------------------");
                            System.out.println("Tekan apa saja untuk kembali ke menu utama");
                            pilihan = compareSomeinput(myObj.nextLine());
                            System.out.print("\n");
                            break;
                        case "0":
                            pilihan = "3";
                            break;
                        default:
                            break;
                    }
                    break;
                case "0":
                    System.out.println("----------------");
                    System.out.println("Program di tutup");
                    System.out.println("----------------");
                    flagLoop = false;
                    break;
                default:
                    mainMenu();
                    pilihan = myObj.nextLine();
                    System.out.print("\n");

                    break;

            }
        }
    }
    public static void mainMenu(){
        System.out.println("-------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("-------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume Bidang");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("-------------------------------------");
        System.out.print("Masukkan pilihan: ");
    }
    public static String compareSomeinput(String a){
        if (a.equals("1") || a.equals("2") || a.equals("3")) {
            return "a";
        }
        else return a;
    }
}
